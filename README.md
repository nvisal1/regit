# README #
Nicholas Visalli
2/6/18

REGIT -> My first Ruby on Rails Project 

### What is this repository for? ###

* My first project using Ruby on Rails. It is a Reddit-like site that is based 
* off of a project found at https://www.youtube.com/watch?v=7-1HCWbu7iU&t=3314s

* Project version 1.0 (I plan to add more features and create a custom front end eventually) 

### How do I get set up? ###

* Open project in any text editor 
* In terminal -> cd, then run rails s or rails server to start server
* Open in browser, should be on localhost:3000 

### Contribution guidelines ###

* N/A -> personal project

